#!/bin/bash
set -ex

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

TEMPLATE_DIR=${DIR}/template


if [ -z "${NAMESPACE}" ]; then
  echo "Please set the NAMESPACE env variable (with the namespace name of the IAM deployment)!"
  exit 1
fi

if [ -z "${IAM_HOST}" ]; then
  echo "Please set the IAM_HOST env variable (with the fqdn of the IAM deployment)!"
  exit 1
fi

if [ -z "${IAM_ORGANISATION_NAME}" ]; then
  echo "Please set the IAM_ORGANISATION_NAME env variable!"
  exit 1
fi

if [ -z "${VOMS_ORGANISATION_NAME}" ]; then
  echo "Please set the VOMS_ORGANISATION_NAME env variable!"
  exit 1
fi

if [ -z "${VOMS_HOST}" ]; then
  echo "Please set the VOMS_HOST env variable (with the fqdn of the VOMS deployment)!"
  exit 1
fi

TARGET_DIR=${TARGET_DIR:-${DIR}/overlays/${IAM_HOST}}

cp -r ${TEMPLATE_DIR} ${TARGET_DIR}

KEY_ID=$(echo ${IAM_HOST}| cut -d"." -f1)

# Globals variables cm setup
sed -e "s#%%IAM_HOST%%#${IAM_HOST}#" \
  -e "s#%%IAM_ORGANISATION_NAME%%#${IAM_ORGANISATION_NAME}#" \
  -e "s#%%VOMS_AA_HOST%%#${VOMS_HOST}#" \
  -e "s#%%VOMS_AA_VO%%#${VOMS_ORGANISATION_NAME}#" \
  ${TEMPLATE_DIR}/iam-global-vars.env > ${TARGET_DIR}/iam-global-vars.env

## VOMS LSC file generation
mkdir -p ${TARGET_DIR}/voms-aa/vomsdir/${VOMS_ORGANISATION_NAME}
sed -e "s#%%IAM_HOST%%#${IAM_HOST}#" ${TEMPLATE_DIR}/voms-aa/vomsdir/template.lsc > ${TARGET_DIR}/voms-aa/vomsdir/${VOMS_ORGANISATION_NAME}/${IAM_HOST}.lsc

LSC_FILE_PATH="${VOMS_ORGANISATION_NAME}/${IAM_HOST}.lsc"

# Kustomization.yaml templating
sed -e "s#%%NAMESPACE%%#${NAMESPACE}#" -e "s#%%LSC_FILE_PATH%%#${LSC_FILE_PATH}#" ${TEMPLATE_DIR}/kustomization.yaml > ${TARGET_DIR}/kustomization.yaml

## JSON web keys generation
sh ${DIR}/utils/jwk/generate-jwks.sh > ${TARGET_DIR}/iam/secrets/json-keystore.jwks

## VOMS NGINX config
sed -e "s#%%VOMS_HOST%%#${VOMS_HOST}#" ${TEMPLATE_DIR}/voms-aa/nginx/conf.d/voms-aa.conf > ${TARGET_DIR}/voms-aa/nginx/conf.d/voms-aa.conf

## IAM NGINX config
sed -e "s#%%NAMESPACE%%#${NAMESPACE}#" -e "s#%%IAM_HOST%%#${IAM_HOST}#" ${TEMPLATE_DIR}/iam/nginx/iam.conf > ${TARGET_DIR}/iam/nginx/iam.conf
