#!/bin/bash

openssl x509 -in tls/tls.crt -noout -subject -issuer -nameopt compat | sed -e 's/^subject= //' -e 's/^issuer= //'
