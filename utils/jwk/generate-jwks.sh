#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

JWK_KEY_ID=${JWK_KEY_ID:-rsa1}
JWK_KEY_TYPE=${JWK_KEY_TYPE:-RSA}
JWK_KEY_SIZE=${JWK_KEY_SIZE:-2048}

java -jar ${DIR}/jwk-gen.jar -t ${JWK_KEY_TYPE} -s ${JWK_KEY_SIZE} -S -i ${JWK_KEY_ID} | tail -n +2 
